package myapplication;

import mylibrary.Calculator;

public class App {
    public static int add(int summand1, int summand2) {
        Calculator calc = new Calculator();
        return calc.add(summand1, summand2);
    }

    public static void main(String[] args) {
        System.out.println(add(1,2));
        System.out.println(add(1,5));
        System.out.println(add(1,10));
    }
}
