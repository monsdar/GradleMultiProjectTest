
import mylibrary.Calculator;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
    @Test public void testAddition() {
        Calculator calc = new Calculator();
        assertEquals(calc.add(1,2), 3);
        assertEquals(calc.add(-1,-2), -3);
        assertEquals(calc.add(-1,2), 1);
    }

    @Test public void testSubtraction() {
        Calculator calc = new Calculator();
        assertEquals(calc.subtract(1,2), -1);
        assertEquals(calc.subtract(-1,-2), 1);
        assertEquals(calc.subtract(-1,2), -3);
    }
}
