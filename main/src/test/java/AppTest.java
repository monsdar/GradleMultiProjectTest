
import myapplication.App;

import org.junit.Test;
import static org.junit.Assert.*;

public class AppTest {
    @Test public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertEquals(App.add(1,2), 3);
    }
}
