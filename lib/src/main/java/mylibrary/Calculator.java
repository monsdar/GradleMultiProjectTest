package mylibrary;

public class Calculator {
    public int add(int summand1, int summand2) {
        return summand1 + summand2;
    }
    public int subtract(int minuend, int subtrahend) {
        return minuend - subtrahend;
    }
}
